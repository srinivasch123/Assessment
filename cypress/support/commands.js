// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This is will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('login',()=>{
    cy.visit('https://github.com/login', {
            onBeforeLoad: (win) => {
              win.sessionStorage.clear()
            }
          })
    cy.get('#login_field').type('srinivassaravankumar@gmail.com')
    cy.get('#password').type('srini@789')
    cy.get("[type='submit']").click()
    expect(".avatar.float-left.mr-1").to.exist
})
Cypress.Commands.add('logout',()=>{
    cy.visit('https://github.com')
    cy.get('summary.HeaderNavlink.name.mt-1').click()
    cy.get('button.dropdown-item.dropdown-signout').contains('Sign out').click()
    expect("#user[email]").to.exist
})