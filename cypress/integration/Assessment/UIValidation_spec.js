describe('Github UI Validation', function (){
    
    beforeEach(function(){
        cy.visit(Cypress.env("baseURL")+'/login');
    })    

    it('Test - Sign in to Gut hub text validation', function(){
        cy.get('.auth-form-header.p-0').should('contain',"Sign in to GitHub");
    })

    it('Test - Username Field validation', function(){
        expect('#login_field').to.exist;
    })

    it('Test - Password Field validation', function(){
        expect('#password').to.exist;
    })

    it('Test - Sign in Field validation', function(){
        expect("[type='submit']").to.exist;
    })

    it('Test - Invalid email login validation', function(){
        cy.get('#login_field').type('srini@abc.com');
        cy.get("[type='submit']").click();
        cy.get(".container").should('contain',"Incorrect username or password.");
    })

    it('Test - Valid email and invalid pwd login validation', function(){
        cy.get('#login_field').type(Cypress.env("emaiId"));        
        cy.get('#password').type('abc@123');
        cy.get("[type='submit']").click();
        cy.get(".container").should('contain',"Incorrect username or password.");
    })


    it('Test-Successful Login validation', function(){
        cy.get('#login_field').type(Cypress.env("emaiId"));
        cy.get('#password').type(Cypress.env("pwd"));
        cy.get("[type='submit']").click();
        expect(".avatar.float-left.mr-1").to.exist;
        cy.logout();
    })

    it('Test - Create Valid Repository', function(){
        cy.login();
        cy.get('a.btn.btn-sm.btn-primary.text-white').click();
        cy.get('#repository_name').type("NewRepo");
        cy.get('#repository_description').type("A New Repository");
        cy.get('#repository_public_true').click();
        cy.get("button.btn.btn-primary.first-in-line").click();
        cy.get("p.mb-0").should('contain',"We recommend every repository include a");
        cy.logout();
    })

    it('Test - Duplicate Repository', function(){
        cy.login();
        cy.get('a.btn.btn-sm.btn-primary.text-white').click();
        cy.get('#repository_name').type("NewRepo");
        cy.get('#repository_description').type("A New Repository");
        cy.get('#repository_public_true').click();
        cy.get("button.btn.btn-primary.first-in-line").click();
        cy.get("dd.error").should('contain',"already exists on this account");
        cy.logout();
    })

    it('Test - Create Readme File', function(){
        cy.login();
        cy.get("[title='NewRepo']").click();
        if(cy.get('a').contains("README")){
            cy.get('a').contains("README").click();
            cy.get('#submit-file').click();
            expect(".commit-tease.js-details-container.Details.d-flex").to.exist;
            cy.logout();
        }
        else if (cy.get('a.btn.btn-sm.btn-primary.flash-action').should('contain',"Add a README")){
            cy.get('a.btn.btn-sm.btn-primary.flash-action').click();
            cy.get('#submit-file').click();
            expect(".commit-tease.js-details-container.Details.d-flex").to.exist;
            cy.logout();
        }
    })

    it('Test - Delete Repo', function(){
        cy.login();
        cy.get("[title='NewRepo']").click();        
        cy.get("[data-selected-links='repo_settings repo_branch_settings hooks integration_installations repo_keys_settings issue_template_editor /srinivasch123/NewRepo/settings']").click();
        cy.get('summary.btn.btn-danger.boxed-action').contains('Delete this repository').click();
        cy.get("[aria-label='Type in the name of the repository to confirm that you want to delete this repository.']").type('NewRepo');
        cy.get('button.btn.btn-block.btn-danger').contains('I understand the consequences, delete this repository').click();
        cy.get("[title]").contains('NewRepo').should('not.exist');
        cy.logout();
    })

})