describe('API Validation', function (){
    
    it('Test - API for create repo', function(){
        
        cy.request({
            method:'POST',
            url:Cypress.env("apihost")+'/user/repos',
            headers: {
                "Authorization": "Basic c3Jpbml2YXNjaDEyMzpzcmluaUA3ODk="
            },
            body: {
                    name: 'APIRepo', 
                    "auto_init": false, 
                    "private": false, 
                    "gitignore_template": "nanoc" 
            }            
        }).its('status').should('equal',201);
    })

    it('Test - API for create readme', function(){
        
        cy.request({
            method:'PUT',
            url:Cypress.env("apihost")+'/repos/Srinivasch123/APIRepo/contents/ReadMe.md',
            headers: {
                "Authorization": "Basic c3Jpbml2YXNjaDEyMzpzcmluaUA3ODk="
            },
            body: {
                "message": "my commit message",
                "committer": {
                  "name": "Srini",
                  "email": "srinivassaravankumar@gmail.com"
                },
                "content": "QSBzYW1wbGUgcmVhZG1lIGZpbGU="
            }          
        }).its('status').should('equal',201);
    })

    it('Test - API for delete repo', function(){
        
        cy.request({
            method:'DELETE',
            url:Cypress.env("apihost")+'/repos/SrinivasCh123/APIRepo',
            headers: {
                "Authorization": "Basic c3Jpbml2YXNjaDEyMzpzcmluaUA3ODk="
            }            
        }).its('status').should('equal',204);
    })
})