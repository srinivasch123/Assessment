This project is created for performing an assessment on cypress framework.

Steps to run this project:
1. Navigate to https://gitlab.com/srinivasch123/Assessment/ 
2. Fork or clone the project
3. Navigate to CI tab and create a pipeline 
4. Run the pipeline. 
5. This CI tests include both UI and API validations, however the API validation is fialing as Gitlab is not allowing Bearer Authentication. The API scripts would run manually without CI.
6. APIValidation_spec.js contains the API validations for creating a repo, create a read me and delete repo. I have included validations to check response codes once API call are performed.
7. UIValidation_spec.js contains the UI vaidations for logging into Github, creating a repo, create a read me and delete repo. Including various validations like on launch of the page if the necessary fields are present, negatie tests like adding a duplicate repository and happy flow.